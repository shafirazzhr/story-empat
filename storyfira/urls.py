"""storyfira URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from matakuliah import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('profile/', include('story1.urls')),
    path('pengalaman/', include('story4.urls')),
    path('hobi/', include('hobi.urls')),
    path('addmatkul/', views.addmatkul),
    path('savematkul/', views.savematkul),
    path('tampilkanmatkul/', include('matakuliah.urls')),
    path('hapusmatkul/<int:id>',views.hapus, name='hapusmatkul'),
    path('satumatkul/<int:id>',views.satu, name='satumatkul'),
]
