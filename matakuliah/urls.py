from django.urls import include, path


from . import views
from .views import tampilkanmatkul

app_name = 'matakuliah'

urlpatterns = [
   path('', views.tampilkanmatkul, name='tampilkanmatkul'),
   
]
