from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .forms import MatkulForm
from .models import Matkul
# Create your views here.
response = {}


def addmatkul(request):
	form = MatkulForm()
	return render(request,'addmatkul.html',{'form':form})

def savematkul(request):
	form = MatkulForm(request.GET or None)
	if (form.is_valid() and request.method == 'GET'):
		form.save()
		return HttpResponseRedirect('/tampilkanmatkul')
	else:
		return HttpResponse("Data tidak valid")

def tampilkanmatkul(request):
	matkul = Matkul.objects.all()
	response['matkul'] = matkul
	html = 'tampilkanmatkul.html'
	return render(request, html, response)

def hapus(request,id):
	matkul = Matkul.objects.get(id=id)
	matkul.delete()
	return HttpResponseRedirect('/tampilkanmatkul')

def satu(request,id):
	matkul = Matkul.objects.get(id=id)
	response['matkul'] = matkul
	html = 'satumatkul.html'
	return render(request, html, response)




	 
