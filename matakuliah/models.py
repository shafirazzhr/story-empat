from django.db import models


class Matkul(models.Model):
    nama_matkul= models.CharField(max_length=30)
    dosen_pengajar= models.CharField(max_length=30,default="-")
    jumlah_sks= models.CharField(max_length=2,default="0")
    deskripsi_matkul= models.CharField(max_length=30,default="-")
    semester_tahun=models.CharField(max_length=30,default="-")
    ruang_kuliah=models.CharField(max_length=30,default="-")