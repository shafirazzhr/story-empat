from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'
    error_messages = {
        'required':'Please Type'
    }

    nama_matkul= forms.CharField(required=True)
    dosen_pengajar= forms.CharField(required=True)
    jumlah_sks= forms.CharField(required=True,max_length=True)
    deskripsi_matkul= forms.CharField(required=True, widget=forms.Textarea)
    semester_tahun=forms.CharField(required=True)
    ruang_kuliah=forms.CharField(required=True)

 
