from django.urls import include, path


from . import views
from .views import profile

app_name = 'story1'

urlpatterns = [
   path('', views.profile, name='profile'),
]