# storyfira

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

Repositori ini berisi sebuah templat untuk membuat proyek Django yang siap
di-*deploy* ke Heroku melalui GitLab CI untuk keperluan story mata kuliah PPW 2020/2021.


## Daftar isi

- [Daftar isi](#daftar-isi)
- [Profil](#profile)
- [Lisensi](#lisensi)

## Profile

Nama: Shafira Azzahra
NPM: 1906353813
Kelas: PPW-G



## Lisensi

Templat ini didistribusikan dengan lisensi [The Unlicense][license]. Proyek
yang dibuat dengan templat ini dipersilakan untuk didistribusikan dengan
ketentuan yang berbeda.

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/laymonage/django-template-heroku/-/commits/master
[readme-en]: README.en.md
[heroku-dashboard]: https://dashboard.heroku.com
[djecrety]: https://djecrety.ir
[account-settings]: https://dashboard.heroku.com/account
[chromedriver]: https://chromedriver.chromium.org/downloads
[homebrew]: https://brew.sh
[ticket-21227]: https://code.djangoproject.com/ticket/21227
[bypass-cache]: https://en.wikipedia.org/wiki/Wikipedia:Bypass_your_cache
[flake8]: https://pypi.org/project/flake8
[pylint]: https://pypi.org/project/pylint
[black]: https://pypi.org/project/black
[isort]: https://pypi.org/project/isort
[template]: https://docs.djangoproject.com/en/3.1/ref/django-admin/#cmdoption-startproject-template
[repo-gh]: https://github.com/laymonage/django-template-heroku
[repo-gl]: https://gitlab.com/laymonage/django-template-heroku
[license]: LICENSE
